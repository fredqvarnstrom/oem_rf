"""
Python wrapper for XBee Pro 3B Programmable module. (XBP9B-DMSTB002)

Author: Wester de Weerdt
e-mail: wester.de.weerdt@dutchmail.com


"""
from functools import reduce

import serial
import binascii
import time

import sys

try:
    xrange
except NameError:
    xrange = range


class EXbee:

    START_BYTE = "7E"
    ESCAPED_CHARACTER = ["7E", "7D", "11", "13"]
    delivery_status = {"00":  "Success",
                       "01":  "MAC ACK Failure",
                       "02":  "CCA Failure",
                       "15":  "Invalid destination endpoint",
                       "21":  "Network ACK Failure",
                       "22":  "Not Joined to Network",
                       "23":  "Self-addressed",
                       "24":  "Address Not Found",
                       "25":  "Route Not Found",
                       "26":  "Broadcast source failed to hear a neighbor relay the message",
                       "2B":  "Invalid binding table index",
                       "2C":  "Resource error lack of free buffers, timers, etc",
                       "2D":  "Attempted broadcast with APS transmission",
                       "2E":  "Attempted unicast with APS transmission, but EE=0",
                       "32":  "Resource error lack of free buffers, timers, etc.",
                       "74":  "Data payload too large",
                       "75":  "Indirect message unrequested", }

    discovery_status = {"00":  "No Discovery Overhead",
                        "01":  "Address Discovery",
                        "02":  "Route Discovery",
                        "03":  "Address and Route",
                        "04":  "Extended Timeout Discovery"}

    remote_status = {"00": "Status OK",
                     "40": "Unknown status",
                     }

    bytes = []
    fields = []
    frame = []
    b_initialized = False

    b_p3 = False        # indicates python3 or python2

    def __init__(self, port, baud):
        self.baud = baud
        self.port = port
        self.b_initialized = False
        self.ser = serial.Serial(self.port, self.baud, timeout=0.5)
        if sys.version_info >= (3, 0):  # in case of python3
            self.b_p3 = True
        else:
            self.b_p3 = False

    def initialize(self):
        """
        At first, master sends '0x0D0D0D' to get status of Programmable MCU of XBee.
        The type of response contains string such as B-Bypass, F-Update, T-Timeout, V-BL, Ver
        """
        buf = '0D0D0D'
        s_time = time.time()
        while True:
            self.ser.write(binascii.unhexlify(buf))
            if time.time() - s_time < 10:  # Max initializing time is 10 sec
                hex_val = self.toHex(self.read_line())
                # check whether received data contains 'B-', 'F-', 'T-', 'V-'
                if "422d" in hex_val and "462d" in hex_val and "542d" in hex_val and "562d" in hex_val:
                    break
                else:
                    continue
            else:
                print("Initializing is failed, time out...")
                return False

        # End initialization by sending 4242
        self.ser.write(binascii.unhexlify('4242'))
        time.sleep(0.5)

        self.b_initialized = True

        return True

    def set_as_end_device(self, network_id, device_name):
        """
        Set current device as End Device by executing several AT commands.
        :param network_id: Network ID which all XBee modules have same value, 4-byte length.
        :param device_name: Device Name users can see in the Digi Mesh Network. (Up to 20 byte)
        """
        # TODO: Currently this function is not working, must add function of transparent mode
        # TODO: Add error exception code
        self.execute_at('AP', param='02', is_ascii=False)  # API Mode with Escapes
        self.execute_at('CE', param='02', is_ascii=False)  # Routing/Messaging Mode: Non-Routing Module

        self.execute_at('ID', param=network_id, is_ascii=False)             # Network ID, must be same for all XBee
        self.execute_at('PL', param='04', is_ascii=False)                   # TX Power Level : Highest
        self.execute_at('NI', param=device_name)                            # Device name which users can see
        self.execute_at('NO', param='05', is_ascii=False)                   # Network Discovery Options
        # Network Discovery Options, 0x01: Append DD value  0x04: Append RSSI
        self.execute_at('NO', '05', is_ascii=False)
        return True

    def set_as_router(self, network_id, device_name):
        """
        Set current device as Coordinator by executing several AT commands.
        :param network_id: Network ID which all XBee modules have same value, 4-byte length.
        :param device_name: Device Name users can see in the Digi Mesh Network. (Up to 20 byte)
        """
        # TODO: Currently this function is not working, must add function of transparent mode
        # TODO: Add error exception code
        self.execute_at('AP', param='02', is_ascii=False)  # API Mode with Escapes
        self.execute_at('CE', param='01', is_ascii=False)  # Routing/Messaging Mode: Indirect Msg Coordinator

        self.execute_at('ID', param=network_id, is_ascii=False)             # Network ID, must be same for all XBee
        self.execute_at('PL', param='04', is_ascii=False)                   # TX Power Level : Highest
        self.execute_at('NI', param=device_name)                            # Device name which users can see
        self.execute_at('NO', param='05', is_ascii=False)                   # Network Discovery Options
        # Network Discovery Options, 0x01: Append DD value  0x04: Append RSSI
        self.execute_at('NO', '05', is_ascii=False)
        return True

    def read_line(self):
        """
        Read data from serial until time out occurs
        """
        r_buf = ''
        while True:
            try:
                data = self.ser.read()
                if len(data) == 0:
                    break
                if self.b_p3:
                    r_buf += data.decode('utf-8')
                else:
                    r_buf += data

            except serial.SerialException as e:
                time.sleep(0.05)
        return r_buf.strip()

    def add_hex2(self, hex1, hex2):
        return hex(int(hex1, 16) + int(hex2, 16))

    def sub_hex2(self, hex1, hex2):
        return hex(int(hex1, 16) - int(hex2, 16))

    # convert byte to hex
    def xor_hex(self, a, b):
        return '%x' % (int(a, 16) ^ int(b, 16))

    def toHex(self, s):
        """
        Convert ASCII string to HEX string
        """
        # If instance is not iterable, change it to the list
        if not hasattr(s, '__iter__'):
            s = [s, ]

        if len(s) == 0:
            return ''
        lst = []
        for ch in s:
            if self.b_p3:      # in case of python3
                if isinstance(ch, int):
                    hv = "%x" % ch
                else:
                    hv = hex(ord(ch)).replace('0x', '')
            else:                               # python2
                hv = hex(ord(ch)).replace('0x', '')

            if len(hv) == 1:
                hv = '0' + hv
            lst.append(hv)

        return reduce(lambda x, y: x+y, lst)

    # READ DATA OVER RX
    # read the type and the length of incoming frame
    def read_frame_infos(self):
        """
        Wait for delimiter and extract packet length and type following it.
        """
        self.bytes = []
        s_time = time.time()

        while True:
            start_byte = self.ser.read()
            if len(start_byte) > 0:
                s_time = time.time()  # update last received time

                if self.toHex(start_byte).upper() == EXbee.START_BYTE:
                    self.bytes.append(start_byte)
                    i = 1
                    detect = False
                    length_hex = ""
                    while i < 3:
                        cc = self.ser.read()
                        self.bytes.append(cc)
                        length_hex = ""
                        if self.toHex(cc).upper() in EXbee.ESCAPED_CHARACTER:
                            detect = True
                            continue
                        else:
                            if detect:
                                length_hex += self.xor_hex(self.toHex(cc), "20")
                                detect = False
                            else:
                                length_hex += self.toHex(cc)
                            i += 1

                    length = int(length_hex, 16)
                    # read the type of frame
                    type_byte = self.ser.read()
                    self.bytes.append(type_byte)
                    type_hex = self.toHex(type_byte)
                    return {"length": length, "frame_type": type_hex}
                else:
                    time.sleep(0.01)
            else:
                if time.time() - s_time > 10:
                    return {"length": 0, "frame_type": None}

    # read incoming RX Frame
    def read_rx_api(self, f_type):
        while True:
            infos = self.read_frame_infos()
            length = infos["length"]

            if infos["frame_type"] == f_type:
                while length > 0:
                    r_byte = self.ser.read()

                    if self.toHex(self.bytes[-1]) == '7d':  # If previous byte is 7d
                        tmp = self.toHex(r_byte)
                        tmp = self.xor_hex(tmp, '20')
                        if tmp.upper() in self.ESCAPED_CHARACTER:   # If current value is escaped
                            self.bytes[-1] = binascii.unhexlify(tmp)
                            continue
                    self.bytes.append(r_byte)
                    length -= 1

                return self.bytes
            else:
                print("ERROR : received a Wrong frame !!! ")
                return None

    def filter_frame(self):
        self.fields = [self.toHex(self.bytes[k]).upper() for k in range(len(self.bytes))]
        return self.fields

    def export_rx(self):
        """
        Parse received data to dict object
        """
        self.frame = {}
        ss = ""
        for c in self.fields:
            ss += c
        self.frame['DELIMITER'] = ss[0:2]
        self.frame['LENGTH'] = int(ss[2:6], 16)
        self.frame['FRAME_TYPE'] = ss[6:8]
        self.frame['SOURCE_ADDRESS_64'] = ss[8:24]
        self.frame['SOURCE_ADDRESS_16'] = ss[24:28]
        self.frame['RECEIVE_OPTIONS'] = ss[28:30]
        self.frame['DATA_HEX'] = ss[30:len(ss) - 2]

        self.frame['DATA'] = binascii.unhexlify(ss[30:len(ss) - 2])
        if self.b_p3:  # return string
            self.frame['DATA'] = str(self.frame['DATA'], 'utf-8')

        self.frame['CHECKSUM'] = self.fields[len(ss)-2: len(ss)]

    def read_rx(self):
        while True:
            response = self.read_rx_api("90")
            if response is None:
                print("No received packet.")
                return None

            if "ERROR" in response:
                print("Not RX Frame Received !!!! ")
            else:
                self.filter_frame()
                self.export_rx()
                return self.frame

    # SEND DATA OVER TX
    def send_tx(self, data, long_addr, frame_id="01", short_addr="FFFE", broadcast_radius="00", options="00", frame_type="10"):
        """
        Send data to remote node.
        """
        length = 14 + len(data)
        data_hex = self.toHex(data)
        frame = ["7E", format(length, '04x'), frame_type, frame_id, long_addr, short_addr, broadcast_radius, options,
                 data_hex.upper()]
        # calculate checksum
        total = frame_type
        total = self.add_hex2(frame_id, total)[2:4]
        for i in xrange(0, len(long_addr), 2):
            total = self.add_hex2(long_addr[i:i + 2], total)[2:]
        total = self.add_hex2(short_addr[0:2], total)[2:]
        total = self.add_hex2(short_addr[2:4], total)[2:]
        total = self.add_hex2(broadcast_radius, total)[2:]
        total = self.add_hex2(options, total)[2:]
        for i in xrange(0, len(data_hex), 2):
            total = self.add_hex2(data_hex[i:i + 2], total)[2:]
        ss = total[len(total) - 2:len(total)]
        checksum = self.sub_hex2("FF", ss).upper()

        frame.append(checksum[2:4])
        frame2 = ["7E"]
        for i in xrange(1, len(frame) - 1):
            fr_i = frame[i]
            for j in xrange(0, len(fr_i), 2):
                if fr_i[j:j + 2] in ["7D", "7E", "11", "13"]:
                    frame2.append("7D")
                    frame2.append(self.xor_hex("20", fr_i[j:j + 2]))
                else:
                    frame2.append(fr_i[j:j + 2])

        frame2.append(checksum[2:4])

        ss = ""
        for c in frame2:
            ss += c
        self.ser.write(binascii.unhexlify(ss))

        time.sleep(0.01)
        cc = self.ser.read(11)

        try:
            ds = str(self.toHex(cc[8])) + "( " + self.delivery_status[str(self.toHex(cc[8])).upper()] + " )"
        except:
            ds = str(self.toHex(cc[8]))
        try:
            discov_s = str(self.toHex(cc[9])) + "( " + self.discovery_status[str(self.toHex(cc[9])).upper()] + " )"
        except:
            discov_s = str(self.toHex(cc[9]))

        return {"Start_delimiter": self.toHex(cc[0]),
                "Length": self.toHex(cc[1:3]),
                "Frame_type": self.toHex(cc[3]),
                "Frame_id": self.toHex(cc[4]),
                "des.address_16-bit": self.toHex(cc[5:7]),
                "Retry_count": self.toHex(cc[7]),
                "Delivery_status": ds,
                "Discovery_status": discov_s,
                "Checksum": self.toHex(cc[10])
                }

    # AT COMMAND
    def execute_at(self, command, param="", frame_id="01", is_ascii=True):
        """
        Send AT command and return response.
        :param is_ascii: Boolean property which indicates that param is ascii character or not.
        :param frame_id: Frame ID
        :param param: parameter for AT command
        :param command: AT command value
        """

        if len(command) != 2:
            print("Error : verify the Command AT !!!!")
            return None
        else:
            frame_at = ["7E"]
            length = str(format(4+len(param), "04x"))
            frame_at.append(length[0:2])
            frame_at.append(length[2:4])
            frame_at.append("08")
            frame_at.append(frame_id)
            frame_at.append(self.toHex(command)[0:2])
            frame_at.append(self.toHex(command)[2:4])
            if len(param) > 0:
                if is_ascii:
                    for i in range(0, len(self.toHex(param)), 2):
                        pp = self.toHex(param)[i:i+2]
                        frame_at.append(pp)
                else:
                    for i in range(len(param)/2):
                        frame_at.append(param[i*2:i*2+2])

            checksum = "00"
            frame = []
            for i in range(1, len(frame_at)):
                fr_i = frame_at[i]
                for j in range(0, len(fr_i), 2):
                    if fr_i[j:j+2] in ['7D', '7E', '11', '13']:
                        frame.append("7D")
                        frame.append(self.xor_hex("20", fr_i[j:j+2]))
                    else:
                        frame.append(fr_i[j:j+2])

                    if i > 2:
                        checksum = self.add_hex2(fr_i[j:j+2], checksum)

            checksum = self.sub_hex2("FF", checksum[len(checksum)-2: len(checksum)])[2:4]
            frame.append(checksum)

            ss = "7E"
            for c in frame:
                ss = ss+c

            self.ser.write(binascii.unhexlify(ss))
            time.sleep(0.01)
            resp = self.response_at()
            return resp

    def response_at(self):
        while True:
            if self.read_rx_api("88") is None:
                print("Failed to receive data...")
                return None
            status = self.toHex(self.bytes[7])
            if status == "00":
                response = ""
                for i in range(8, len(self.bytes)-1):
                    response += self.toHex(self.bytes[i])
                return response
            else:
                print("Invalid AT command !!!!!")
                return None

    def send_remote_at(self, command, address, param="", dest_16="FFFE", frame_id="01"):

        if len(command) != 2:
            print("Error : verify the Command AT !!!!")
            return None
        else:
            frame_at = []
            ll = 5 + len(param) + len(address)/2 + len(dest_16)/2
            length = str(format(ll, "04x"))
            frame_at.append(length[0:2])
            frame_at.append(length[2:4])
            frame_at.append("17")
            frame_at.append(frame_id)
            for i in xrange(0, len(address), 2):
                frame_at.append(address[i:i+2])
            frame_at.append(dest_16[0:2])
            frame_at.append(dest_16[2:4])
            frame_at.append("02")

            frame_at.append(self.toHex(command)[0:2])
            frame_at.append(self.toHex(command)[2:4])
            if len(param) > 0:
                for i in range(0, len(self.toHex(param)), 2):
                    pp = self.toHex(param)[i:i+2]
                    frame_at.append(pp)
            checksum = "00"
            frame = []
            for i in range(0, len(frame_at)):
                fr_i = str(frame_at[i])
                for j in range(0, len(fr_i), 2):
                    if fr_i[j:j+2] in EXbee.ESCAPED_CHARACTER:
                        frame.append("7D")
                        frame.append(self.xor_hex("20", fr_i[j:j+2]))
                    else:
                        frame.append(fr_i[j:j+2])

                    if i > 1:
                        checksum = self.add_hex2(fr_i[j:j+2], checksum)

            checksum = self.sub_hex2("FF", checksum[len(checksum)-2: len(checksum)])[2:4]
            frame.append(checksum)
            ss = "7E"
            for c in frame:
                ss = ss + c
            self.ser.write(binascii.unhexlify(ss))
            time.sleep(0.01)
            return self.response_remote_at()

    def response_remote_at(self):
        while True:
            self.read_rx_api("97")
            status = self.toHex(self.bytes[19])
            if status == "00":
                response = ""
                for i in range(20, len(self.bytes)-1):
                    response += self.toHex(self.bytes[i])
                return response
            else:
                print("response error.")
                return None

    def get_device_id(self):
        """
        Get device ID by querying 'NI' command
        """
        hex_val = self.execute_at("NI")
        if hex_val is not None:
            if self.b_p3:  # in case of python3
                result = bytes.fromhex(hex_val).decode('utf-8')
            else:
                result = hex_val.decode("hex")
            return result
        else:
            print("Failed to receive packet, please try again later.")
            return None

    def set_device_id(self, new_id):
        """
        Set new Device ID.
        """
        return self.execute_at("NI", new_id)

    def find_neighbors(self):
        """
        Discovers and reports all RF modules found within immediate RF range.
        The following information is reported for each module discovered.
        MY<CR> (always 0xFFFE)
        SH<CR>
        SL<CR>
        NI<CR> (Variable length)
        PARENT_NETWORK ADDRESS<CR> (2 Bytes) (always 0xFFFE)
        DEVICE_TYPE<CR> (1 Byte: 0=Coord, 1=Router, 2=End Device)
        STATUS<CR> (1 Byte: Reserved)
        PROFILE_ID<CR> (2 Bytes)
        MANUFACTURER_ID<CR> (2 Bytes)
        DIGI DEVICE TYPE<CR> (4 Bytes. Optionally included based on NO settings).
        RSSI OF LAST HOP<DR> (1 Byte. Optionally included based on NO settings.)

        Sample response: 'fffe 0013a20040db7ba6 4d617374657200 fffe 00 00 c105 101e 000b0000 28'

        """
        # resp = 'fffe0013a20040db7ba64d617374657200fffe0000c105101e000b000028'

        resp = self.execute_at("FN")
        if resp is None:
            return None
        # print(resp)
        # parse received packet
        dest_mac = resp[4:20]
        # Next field is NI of source node(variable length), but after it there is a constant value of 'fffe'
        next_delimiter = resp.index('fffe', 1)

        # TODO: not sure why 1 byte is added at the end of NI
        tmp = resp[20:next_delimiter-2]
        if self.b_p3:  # in case of python3
            dest_id = bytes.fromhex(tmp).decode('utf-8')
        else:
            dest_id = tmp.decode('hex')

        dest_type = resp[next_delimiter+4:next_delimiter+6]
        dest_profile_id = resp[next_delimiter+8:next_delimiter+12]
        dest_manufacturer_id = resp[next_delimiter+12:next_delimiter+16]
        dest_digi_type = resp[next_delimiter+16:next_delimiter+24]
        dest_rssi = -int(resp[next_delimiter+24:next_delimiter+26], 16)

        return {'dest_mac': dest_mac,
                'dest_id': dest_id,
                'dest_type': dest_type,
                'dest_profile_id': dest_profile_id,
                'dest_manufacturer_id': dest_manufacturer_id,
                'dest_digi_type': dest_digi_type,
                'dest_rssi': dest_rssi
                }

    def get_64bit_addr(self):
        sh = self.execute_at('SH')
        sl = self.execute_at('SL')
        addr = sh + sl
        return addr


if __name__ == '__main__':
    a = EXbee('COM6', 9600)

    # print a.xor_hex('33', '20')

    # msg = 'fffe007d33a20040db7ba64d617374657200fffe0000c105101e000b000028'
    # b = [msg[i * 2:i * 2 + 2] for i in range(len(msg) / 2)]
    # print a.remove_escape(b)

